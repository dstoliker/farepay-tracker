from flask import Flask,render_template,request,url_for,redirect,jsonify
from flask_sqlalchemy import SQLAlchemy
from bs4 import BeautifulSoup
from urllib import urlopen,urlencode
from httplib import HTTPSConnection
from datetime import datetime
from datetime import date
import calendar
from workdays import networkdays
import ssl

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///farepay.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.jinja_env.add_extension('jinja2.ext.do')
db = SQLAlchemy(app)

class Preference(db.Model):
    preference_id = db.Column(db.Integer, primary_key=True)
    costPerTrip = db.Column(db.Float)
    pushoverAPIKey = db.Column(db.String)
    pushoverUserKey = db.Column(db.String)

    def __init__(self,costPerTrip,pushoverAPIKey,pushoverUserKey):
        self.costPerTrip = costPerTrip
        self.pushoverAPIKey = pushoverAPIKey
        self.pushoverUserKey = pushoverUserKey

class Card(db.Model):
    card_id = db.Column(db.Integer, primary_key=True)
    cardNumber = db.Column(db.String(16), unique=True)
    description = db.Column(db.String(120), unique=True)
    lastBalance = db.Column(db.Float)
    lastUpdate = db.Column(db.DateTime)
    tripsRemaining = db.Column(db.Integer)
    makeEvenAmount = db.Column(db.Float)
    active = db.Column(db.Boolean)
    companyCard = db.Column(db.Boolean)

    def __init__(self,cardNumber,description,active=True,companyCard=True):
        self.cardNumber = cardNumber
        self.description = description
        self.active = active
        self.companyCard = companyCard
        self.updateBalance()

    def __repr__(self):
        return '%s - %s - $%s' % (self.cardNumber, self.description, self.lastBalance)

    def updateBalance(self):
        url = 'https://farepay.rideuta.com/cardActivity.html?cardNum=%s' % self.cardNumber
        # For some reason, this call started to fail because the SSL cert wasn't verified
        # We're just fetching a balance - not terribly critical that SSL is verified
        # See http://stackoverflow.com/questions/27835619/ssl-certificate-verify-failed-error
        context = ssl._create_unverified_context()
        try:
            webreply = urlopen(url,context=context).read()
            soup = BeautifulSoup(webreply,'html.parser')
            #The balance is in the 4th td, we need to strip off the '$'
            balance = soup.div.table.find_all('td')[3].b.contents[0][1:]
            self.lastBalance = float(balance)
            self.lastUpdate = datetime.now()
            pref = Preference.query.first()
            costPerTrip = pref.costPerTrip
            numTripsRemaining = self.lastBalance/costPerTrip
            self.tripsRemaining = int(numTripsRemaining)
            if self.lastBalance % costPerTrip == 0:
                self.makeEvenAmount = 0
            else:
                # Farepay's minimum order is $5
                minimumOrder = 5.0
                leftOver = self.lastBalance - (self.tripsRemaining * costPerTrip)
                needToAdd = costPerTrip * 2 - leftOver
                if needToAdd < minimumOrder:
                    needToAdd += costPerTrip
                self.makeEvenAmount = needToAdd
        except:
            self.lastBalance = 0
            self.makeEvenAmount = 0
            self.lastUpdate = datetime.now()
            self.tripsRemaining = 0

def notifyUser(message):
    pref = Preference.query.first()
    conn = HTTPSConnection("api.pushover.net:443")
    conn.request("POST", "/1/messages.json",
        urlencode({
            "token": pref.pushoverAPIKey,
            "user": pref.pushoverUserKey,
            "message": message,
            "sound": "cashregister",
        }), { "Content-type": "application/x-www-form-urlencoded" })
    reply = conn.getresponse()

def monthInfo():
    now = datetime.now()
    year = now.year
    month = now.month
    day = now.day
    lastDayOfMonth = calendar.monthrange(year,month)[1]
    monthWorkDays = networkdays(date(year,month,1),date(year,month,lastDayOfMonth))
    remainingWorkDays = networkdays(date(year,month,day),date(year,month,lastDayOfMonth))
    output = {'year': year, 'month': month, 'day': day, 'monthWorkDays': monthWorkDays, 'remainingWorkDays': remainingWorkDays}
    return output

# Routes

@app.route('/')
def home():
    month_info = monthInfo()
    active_cards = Card.query.filter(Card.active).all()
    inactive_cards = Card.query.filter(~Card.active).all()
    pref = Preference.query.one_or_none()
    costPerTrip = pref.costPerTrip
    return render_template('index.html', active_cards=active_cards, inactive_cards=inactive_cards, monthInfo=month_info, costPerTrip = costPerTrip, title='Home')

@app.route('/about')
def about():
    return render_template('about.html', title='About')

@app.route('/update')
def update():
    cards = Card.query.filter(Card.active).all()
    for card in cards:
        oldBalance = card.lastBalance
        card.updateBalance()
        if oldBalance != card.lastBalance:
            difference = abs(card.lastBalance - oldBalance)
            message = "%s balance changed from $%.2f to $%.2f (a $%.2f difference). You have %s trips remaining." %(
                card.description,
                oldBalance,
                card.lastBalance,
                difference,
                card.tripsRemaining)
            notifyUser(message)
    db.session.commit()
    return "Success"

@app.route('/add', methods=['POST'])
def addcard():
    cardNumber = request.form['cardNumber']
    description = request.form['cardDescription']
    if 'companyCard' in request.form.keys():
        companyCard = True
    else:
        companyCard = False
    # If this card already exists in the database, update the card instead of creating a new one
    newcard = Card.query.filter(Card.cardNumber == cardNumber).first()
    if newcard == None:
        newcard = Card(cardNumber=cardNumber,description=description,companyCard=companyCard)
    else:
        # Update the card instead
        newcard.description = description
        newcard.active = True
        newcard.updateBalance()
    # the add SHOULD act like upsert... we'll see
    db.session.add(newcard)
    db.session.commit()
    return redirect(url_for('home'))

@app.route('/deactivate/<card_id>')
def deactivatecard(card_id):
    card = Card.query.filter(Card.card_id == card_id).first()
    card.active = False
    db.session.commit()
    return redirect(url_for('home'))

@app.route('/reactivate/<card_id>')
def reactivatecard(card_id):
    card = Card.query.filter(Card.card_id == card_id).first()
    card.active = True
    db.session.commit()
    return redirect(url_for('home'))

@app.route('/_cardinfo', methods=['POST'])
def ajaxCardInfo():
    card = Card.query.filter(Card.card_id == request.form['id']).first()
    return render_template('cardInfoDialog.html', card=card)

if __name__ == '__main__':
    app.run(debug=True)


