# Farepay Tracker

This a Python web application using Flask that assists in tracking the balance of Farepay cards. It's useful for me, but also gives me a reason to dabble in several technologies. It's pretty specialized to my specific use case, but it could be adapted to more general purpose.

## Requirements
* Mobile notifications are handled through PushOver - you'll need an account, the app, and an API key.
* This is only useful if you have and use a Farepay card.

## TODO - Improvements I'm considering
* Consider adding legends to the charts. I like (this idea)[http://jsfiddle.net/vrwjfg9z/]
* Form validation - card number should be a 16 digit integer
* Additionally, the card input should just pay attention to the last 16 digits so that one could enter the card number with a bar code scanner
* Error handling - handle an invalid card or an error while trying to get the balance
* Add a manual update button to the main page
* Track the card what was last used - send an alert that it's time to retire/return that company card
